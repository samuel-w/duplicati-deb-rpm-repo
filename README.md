# duplicati-deb-rpm-repo

Always up-to-date [Duplicati](https://github.com/duplicati/duplicati) repository. Beta and canary branches available.

[![Daily Update Status](https://gitlab.com/samuel-w/duplicati-deb-rpm-repo/badges/master/pipeline.svg)](https://gitlab.com/samuel-w/duplicati-deb-rpm-repo/commits/master)

Based on paulcarroty's work at https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo.

## How to install for RPM-based Linux distributions

### Step 1 - Add repository

Add the repository

#### Fedora/RHEL:
##### Beta:
```sh
sudo tee -a /etc/yum.repos.d/duplicati.repo << 'EOF'
[gitlab.com_samuel-w_repo]
name=gitlab.com_samuel-w_repo
baseurl=https://samuel-w.gitlab.io/duplicati-deb-rpm-repo/rpms-beta/
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://gitlab.com/samuel-w/duplicati-deb-rpm-repo/raw/master/pub.gpg
metadata_expire=1h
EOF
```
##### Canary:
```sh
sudo tee -a /etc/yum.repos.d/duplicati.repo << 'EOF'
[gitlab.com_samuel-w_repo]
name=gitlab.com_samuel-w_repo
baseurl=https://samuel-w.gitlab.io/duplicati-deb-rpm-repo/rpms-can/
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://gitlab.com/samuel-w/duplicati-deb-rpm-repo/raw/master/pub.gpg
metadata_expire=1h
EOF
```

#### openSUSE/SUSE:
##### Beta:
```sh
sudo tee -a /etc/zypp/repos.d/duplicati.repo << 'EOF'
[gitlab.com_samuel-w_repo]
name=gitlab.com_samuel-w_repo
baseurl=https://samuel-w.gitlab.io/duplicati-deb-rpm-repo/rpms-beta/
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://gitlab.com/samuel-w/duplicati-deb-rpm-repo/raw/master/pub.gpg
metadata_expire=1h
EOF
```
##### Canary:
```sh
sudo tee -a /etc/zypp/repos.d/duplicati.repo << 'EOF'
[gitlab.com_samuel-w_repo]
name=gitlab.com_samuel-w_repo
baseurl=https://samuel-w.gitlab.io/duplicati-deb-rpm-repo/rpms-can/
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://gitlab.com/samuel-w/duplicati-deb-rpm-repo/raw/master/pub.gpg
metadata_expire=1h
EOF
```

### Step 2 - Install app

Then type `duplicati` in [GNOME Software](https://wiki.gnome.org/Apps/Software) or use your package manager:

```sh
dnf install duplicati
zypper in duplicati
```




## How to install for **Debian/Ubuntu/Linux Mint**


### Option 1 - (Recommended)

- Add my key:

    ```sh
    wget -qO - https://gitlab.com/samuel-w/duplicati-deb-rpm-repo/raw/master/pub.gpg \
        | gpg --dearmor \
        | sudo dd of=/usr/share/keyrings/duplicati-archive-keyring.gpg
    ```
 
- Add the repository:
    - Beta:
    ```sh
    echo 'deb [ signed-by=/usr/share/keyrings/duplicati-archive-keyring.gpg ] https://samuel-w.gitlab.io/duplicati-deb-rpm-repo/debs-beta duplicati main' \
        | sudo tee /etc/apt/sources.list.d/duplicati.list
    ```
    - Canary:
    ```sh
    echo 'deb [ signed-by=/usr/share/keyrings/duplicati-archive-keyring.gpg ] https://samuel-w.gitlab.io/duplicati-deb-rpm-repo/debs-can duplicati main' \
        | sudo tee /etc/apt/sources.list.d/duplicati.list
    ```
- Update then install duplicati:

    ```sh
    sudo apt update
    sudo apt install duplicati
    ```

- Then search for `duplicati` and run it (e.g. the Activities menu from the Gnome Panel, or whatever else you use as your launcher or application manager).


### Option 2 - (Alternative) use `apt-key`

If `software-properties-common` package is available, you can use `apt-add` and `apt-add-repository` to add the repository and its key.
- Beta:
```sh
wget -qO - https://gitlab.com/samuel-w/duplicati-deb-rpm-repo/raw/master/pub.gpg | sudo apt-key add -
sudo apt-add-repository 'deb [ signed-by=/etc/apt/trusted.gpg.d/duplicati-archive-keyring.gpg ] https://samuel-w.gitlab.io/duplicati-deb-rpm-repo/debs-beta/ duplicati main'
sudo apt update
sudo apt install duplicati
```
- Canary:
```sh
wget -qO - https://gitlab.com/samuel-w/duplicati-deb-rpm-repo/raw/master/pub.gpg | sudo apt-key add -
sudo apt-add-repository 'deb [ signed-by=/etc/apt/trusted.gpg.d/duplicati-archive-keyring.gpg ] https://samuel-w.gitlab.io/duplicati-deb-rpm-repo/debs-can/ duplicati main'
sudo apt update
sudo apt install duplicati
```

### Notes for **Debian/Ubuntu/Linux Mint**:

- If your machine is behind the corporate firewall or proxy

    ```sh
    echo 'Acquire { https::Verify-Peer false }' >>/etc/apt/apt.conf.d/99verify-peer.conf
    ```

### Changing Branches

If you ever want to change from beta to canary or canary to beta, open your repository file and replace "beta" with "can" or vice-versa. Downgrading from canary to beta might not be supported, check the [releases](https://github.com/duplicati/duplicati/releases) page for more information.

## Verification

Checksum verification doesn't work because GPG signature changes the size of packages.
You can use `diff -r` for extracted packages or [pkgdiff](https://github.com/lvc/pkgdiff).

## Forking

If you want to fork this, make sure to change the keys.tar.enc. Create a tar with a public and private GPG key (preferably subkey) with this folder structure:
```
keys/pub.gpg
keys/priv.gpg
```
priv.gpg must not have a password on it. Encrypt it with openssl and  set the password as a CI variable. Make the pub.gpg the same as keys/pub.gpg to allow users to download your key.

## Updates?

[Every](https://gitlab.com/samuel-w/duplicati-deb-rpm-repo/-/pipelines) 48 hours at 23:00 UTC.
