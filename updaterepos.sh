#!/bin/sh
DOWNLOAD=$(curl -s https://api.github.com/repos/duplicati/duplicati/releases)
LATEST_CANARY=`echo "$DOWNLOAD" | grep canary | grep -oP '"tag_name": "\K(.*)(?=")' | head -n1`
LATEST_BETA=`echo "$DOWNLOAD" | grep beta | grep -oP '"tag_name": "\K(.*)(?=")' | head -n1`

echo -e "\e[0;32mDownload new RPM packages"
mkdir -p pkgs/rpms-can pkgs/rpms-beta

echo "$DOWNLOAD" \
  | grep browser_download_url \
  | grep canary \
  | grep '.rpm' \
  | head -n1 \
  | cut -d '"' -f 4 \
  | xargs -n 1 curl --fail -L -o pkgs/rpms-can/duplicati-$LATEST_CANARY-noarch.rpm \
  || { echo "Failed to download rpm"; exit 1; }
  
echo "$DOWNLOAD" \
  | grep browser_download_url \
  | grep beta \
  | grep '.rpm' \
  | head -n1 \
  | cut -d '"' -f 4 \
  | xargs -n 1 curl --fail -L -o pkgs/rpms-beta/duplicati-$LATEST_BETA-noarch.rpm \
  || { echo "Failed to download rpm"; exit 1; }
  
echo -e "\e[0;32mDownload new DEB packages"
mkdir pkgs/debs-can pkgs/debs-beta

echo "$DOWNLOAD" \
  | grep browser_download_url \
  | grep canary \
  | grep '.deb' \
  | head -n1 \
  | cut -d '"' -f 4 \
  | xargs -n 1 curl --fail -L -o /tmp/duplicati-$LATEST_CANARY-1.deb \
  ||  { echo "Failed to download deb"; exit 1; }

echo "$DOWNLOAD" \
  | grep browser_download_url \
  | grep beta \
  | grep '.deb' \
  | head -n1 \
  | cut -d '"' -f 4 \
  | xargs -n 1 curl --fail -L -o /tmp/duplicati-$LATEST_BETA-1.deb \
  ||  { echo "Failed to download deb"; exit 1; }
  
# reprepro configuration
mkdir pkgs/debs-can/conf
touch pkgs/debs-can/conf/{option,distributions}
echo 'Codename: duplicati' >> pkgs/debs-can/conf/distributions
echo 'Components: main' >> pkgs/debs-can/conf/distributions
echo 'Architectures: amd64 arm64 armhf i386 ppc63el s390x' >> pkgs/debs-can/conf/distributions
echo 'SignWith: FF3836CF' >> pkgs/debs-can/conf/distributions

mkdir pkgs/debs-beta/conf
touch pkgs/debs-beta/conf/{option,distributions}
echo 'Codename: duplicati' >> pkgs/debs-beta/conf/distributions
echo 'Components: main' >> pkgs/debs-beta/conf/distributions
echo 'Architectures: amd64 arm64 armhf i386 ppc63el s390x' >> pkgs/debs-beta/conf/distributions
echo 'SignWith: FF3836CF' >> pkgs/debs-beta/conf/distributions


#mkdir -p pkgs/debs/dists/vscodium/
#mkdir -p pkgs/debs/pool/main/a/vscodium/
#mv /tmp/*.deb pkgs/debs/pool/main/a/vscodium/
#dpkg-scanpackages pkgs/debs/pool/main/a/vscodium/ | gzip -9c > pkgs/debs/dists/vscodium/Packages.gz

echo -e "\e[0;32mSign the repositories"
# extract the public and private GPG keys from encrypted archive keys.tar with 
# the secret openssl pass KEY, which is stored in GitlabCI variables
openssl aes-256-cbc -d -in keys.tar.enc -out keys.tar -pbkdf2 -k $KEY
tar xvf keys.tar
#signing the repository metadata with my personal GPG key
gpg2 --import keys/pub.gpg && gpg2 --import keys/priv.gpg
expect -c "spawn gpg2 --edit-key 608AEE7C9A46BCE15E557B28F0CEC762FF3836CF trust quit; send \"5\ry\r\"; expect eof"

rpm --define "_gpg_name Samuel Woon <samuel.woon@protonmail.com>" --addsign pkgs/rpms-can/*rpm
rpm --define "_gpg_name Samuel Woon <samuel.woon@protonmail.com>" --addsign pkgs/rpms-beta/*rpm

# generate and sign RPM repository
# createrepo pkgs/rpms/         # obsoleted tool
createrepo_c --database --compatibility pkgs/rpms-can/
gpg2 --local-user "Samuel Woon <samuel.woon@protonmail.com>" --yes --detach-sign --armor pkgs/rpms-can/repodata/repomd.xml
createrepo_c --database --compatibility pkgs/rpms-beta/
gpg2 --local-user "Samuel Woon <samuel.woon@protonmail.com>" --yes --detach-sign --armor pkgs/rpms-beta/repodata/repomd.xml

# generation of new deb repository
reprepro -V -b pkgs/debs-can includedeb duplicati /tmp/duplicati-$LATEST_CANARY-1.deb
reprepro -V -b pkgs/debs-beta includedeb duplicati /tmp/duplicati-$LATEST_BETA-1.deb


echo -e "\e[0;32mList of imported public and private keys:"
gpg2 --list-keys && gpg2 --list-secret-keys

echo -e "\e[0;32mStarting the deployment"
# clone the origin repo again in different directory
# git clone https://paulcarroty:$GITLAB_API_KEY@gitlab.com/paulcarroty/vscodium-deb-rpm-repo.git newrepo
# cd newrepo && git checkout -b repos
# delete old RPM and Deb packages because they use a lot of storage 
# git filter-branch --tree-filter 'find . -name "*.rpm" -exec rm {} \;'
# git filter-branch --tree-filter 'find . -name "*.deb" -exec rm {} \;'
# copying the downloaded packages in our new git repository 
# rm -rf debs rpms && mv ../pkgs/* .
# commit and forcing update to origin repository and branch 'repos'
# the different branch for packages is used to prevent CI loop
# git add .
# git -c user.name='GitlabCI' -c user.email='gitlab@gitlab.com' commit -m '[ci skip] rebuild the repositories'

# push repo branch to origin
# git push -f https://paulcarroty:$GITLAB_API_KEY@gitlab.com/paulcarroty/vscodium-deb-rpm-repo.git repos 


# DOCS
# https://linux.die.net/man/8/createrepo
# http://manpages.ubuntu.com/manpages/trusty/man1/dpkg-scanpackages.1.html
# https://github.com/circleci/encrypted-files

