#!/bin/sh
sudo tee -a /etc/yum.repos.d/duplicati.repo << 'EOF'
[gitlab.com_samuel-w_repo]
name=gitlab.com_samuel-w_repo
baseurl=https://samuel-w.gitlab.io/duplicati-deb-rpm-repo/rpms-can/
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://gitlab.com/samuel-w/duplicati-deb-rpm-repo/raw/master/pub.gpg
metadata_expire=1h
EOF


REMOTE=`curl -s https://api.github.com/repos/duplicati/duplicati/releases | grep canary | grep -oP '"tag_name": "\K(.*)(?=")' | head -n1 | cut -c 2- | tr -d -`
LOCAL=`dnf list duplicati -y | grep duplicati | tr -s ' ' | cut -d ' ' -f 2 | tr -d -`

if [ "$REMOTE" == "$LOCAL" ]; then
        exit 2;
fi
